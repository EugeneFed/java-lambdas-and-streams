package db;

import java.util.concurrent.CompletableFuture;

public interface DataStorage<T> {
    CompletableFuture<T> getByKey(String key);
}
